package com.raisetime;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.raisetime.utils.Utils;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by STELLENT on 12/27/2017.
 */
@RunWith(AndroidJUnit4.class)
public class SosServiceTest {
    Utils utils=new Utils();
    @Rule
    public ActivityTestRule<MainActivity> activityTestRule=new ActivityTestRule<MainActivity>(MainActivity.class);

    //Test Lat and lon for canada
    @Test
    public void sosLatLonTest() throws Throwable {

        utils.getActivityInstance().runOnUiThread(new Runnable() {
            public void run() {
            //canada
            activityTestRule.getActivity().sosService("56.1304","103.3468");
            }
        });
        Thread.sleep(2000);
    }

    //Test Failure
    @Test
    public void failure() throws Throwable {

        onView(withId(R.id.txtTitle)).check(matches(withText("raisetime")));
    }
}
