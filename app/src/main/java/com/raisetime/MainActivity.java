package com.raisetime;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.raisetime.adapters.ItemArrayAdapter;
import com.raisetime.pojo.Item;
import com.raisetime.rest.ApiClient;
import com.raisetime.rest.ApiInterface;
import com.raisetime.utils.CheckNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    ArrayList<Item> arrayResult;
    RecyclerView recyclerView;
    private StaggeredGridLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ipService();
    }

    private void ipService() {
        ApiInterface apiService =
                ApiClient.getClientCountries().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.callGetCountries();

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {


                    Log.w("2", new Gson().toJson(response.body()));
                    ResponseBody responseBody = (ResponseBody) response.body();
                    //you can do whatever with the response body now...
                    String responseBodyString = null;
                    try {
                        responseBodyString = responseBody.string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        if (CheckNetwork.isOnline(MainActivity.this)) {
                            if (responseBodyString != null && !responseBodyString.equals("null") && !responseBodyString.equals("")) {
                                JSONObject outerJson = new JSONObject(responseBodyString);
                                String latitude = outerJson.getString("lat");
                                String longitude = outerJson.getString("lon");
                                sosService(latitude, longitude);
                                Log.e("responseip", "onResponseip: " + responseBodyString);
                            } else {
                                Toast.makeText(MainActivity.this, "Something wrong please try later.",
                                        Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "No network connection.",
                                    Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(MainActivity.this, "Something wrong please try later.",
                                Toast.LENGTH_LONG).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Something wrong please try later.",
                        Toast.LENGTH_LONG).show();

            }
        });
    }

    public void sosService(String lat, String lon) {
        ApiClient.retrofit = null;
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Log.e("response", "onResponse: " + "aaa");
      /*  RequestBody body =
                RequestBody.create(MediaType.parse("text/plain"), "");*/

        Call<ResponseBody> call = apiService.callSos(lat, lon);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    Log.e("response", "onResponse: " + "bbb");
                    Log.w("2", new Gson().toJson(response.body()));
                    ResponseBody responseBody = (ResponseBody) response.body();
                    //you can do whatever with the response body now...
                    String responseBodyString = null;
                    try {
                        responseBodyString = responseBody.string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        if (CheckNetwork.isOnline(MainActivity.this)) {
                            if (responseBodyString != null && !responseBodyString.equals("null") && !responseBodyString.equals("")) {
                             /*   JSONObject outerJson = new JSONObject(responseBodyString);*/
                                arrayResult = new ArrayList<>();
                                Log.e("response", "onResponse: " + responseBodyString);
                                JSONObject body = new JSONObject(responseBodyString);
                                JSONArray result = body.getJSONArray("response");
                                if (result.length() != 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject raiseTime = result.getJSONObject(i);
                                        //Unix seconds
                                        long unix_seconds = Long.parseLong(raiseTime.getString("risetime"));
                                        //convert seconds to milliseconds
                                        Date date = new Date(unix_seconds*1000L);
                                        // format of the date
                                        SimpleDateFormat jdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z", Locale.getDefault());
                                        jdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));
                                        String javaDate = jdf.format(date);



                                        arrayResult.add(new Item("duration " + raiseTime.getString("duration"), "risetime "+javaDate ));

                                    }
                                    recyclerView();
                                } else {
                                    Toast.makeText(MainActivity.this, "Something wrong please try later.",
                                            Toast.LENGTH_LONG).show();
                                }
                                /*String latitude = outerJson.getString("lat");
                                String longitude = outerJson.getString("lon");
                                sosService(latitude,longitude);*/

                            } else {
                                Toast.makeText(MainActivity.this, "Something wrong please try later.",
                                        Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "No network connection.",
                                    Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(MainActivity.this, "Something wrong please try later.",
                                Toast.LENGTH_LONG).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Something wrong please try later.",
                        Toast.LENGTH_LONG).show();

            }
        });
    }

    private void recyclerView() {


        ItemArrayAdapter itemArrayAdapter = new ItemArrayAdapter(R.layout.list_item, arrayResult);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);

//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(itemArrayAdapter);


    }

}
