package com.raisetime.rest;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("/iss-pass.json")
    public Call<ResponseBody> callSos(@Query("lat") String lat, @Query("lon") String lon);

    @GET("/json")
    public Call<ResponseBody> callGetCountries();








}
